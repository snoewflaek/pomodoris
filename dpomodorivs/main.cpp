#include <thread>
#include <iostream>
#include <cstdlib>
#define _USE_MATH_DEFINES 
#include <math.h> // TODO change into cmath
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "config.h"

using namespace std;

namespace pomo {
	using SFMLEventHandler = std::function<void(sf::Event const &)>;
	using time_point = std::chrono::time_point<std::chrono::steady_clock, std::chrono::duration<double>>;
	using duration = std::chrono::duration<float>;
	using TimeHandler = std::function<void(time_point const &, duration const &)>;

	class SquareProgressBar : public sf::Drawable {
		// TriangleStrip
		vector<sf::Vertex> fill;
		vector<sf::Vertex> line_inner;
		vector<sf::Vertex> line_outer;
		int validsections;
		sf::Transform fit;

	public:
		sf::Transform transform;
		float progress;
		float hollowness;

		SquareProgressBar()
			: fill(10)
			, line_inner(5)
			, line_outer(5)
			, validsections(0)
			, progress(0)
			, hollowness(.8f)
		{
			// global transform of all contained drawables
			transform.translate(100, 100);
			transform.scale(100, 100);
			// grow to fit in the original size
			fit.rotate(5 / 8.0f * 360);
			fit.scale(static_cast<float>(M_SQRT2), static_cast<float>(M_SQRT2));

			updateVertices();
		}

		void updateColor(sf::Color const &color) {
			for (auto &v : fill) v.color = color;
			for (auto &v : line_inner) v.color = color;
			for (auto &v : line_outer) v.color = color;
		}

		void updateVertices() {
			validsections = static_cast<int>(std::floor(progress / .25));
			// reset all positions to defaults
			fill[0].position = sf::Vector2f(1, 0);
			fill[2].position = sf::Vector2f(0, 1);
			fill[4].position = sf::Vector2f(-1, 0);
			fill[6].position = sf::Vector2f(0, -1);
			fill[8].position = fill[0].position;
			for (auto i = 0; i < 5; i++) {
				line_inner[i].position = transform * fit * (hollowness * fill[i * 2].position);
				line_outer[i].position = transform * fit * (fill[i * 2].position);
			}

			// if we need to draw the non-full trapezoid..
			if (fmodf(progress, 0.25f) != 0.0f) {
				auto angle = 2 * static_cast<float>(M_PI) * progress;
				fill[(validsections + 1) * 2].position = sf::Vector2f(
					cos(angle),
					sin(angle)
					) / (abs(cos(angle)) + abs(sin(angle)));
				validsections++;
			}

			// if we have to draw something at all..
			if (validsections != 0) {
				validsections++;

				// the inner vertices are the outer ones, except a little bit closer to (0, 0)
				for (int i = 0; i < 10; i += 2)
					fill[i + 1].position = fill[i].position * hollowness;

				for (int i = 0; i < 10; i++)
					fill[i].position = transform * fit * fill[i].position;
			}
		}

		void draw(sf::RenderTarget &target, sf::RenderStates states) const override
		{
			target.draw(&line_inner[0], 5, sf::LinesStrip, states);
			target.draw(&line_outer[0], 5, sf::LinesStrip, states);
			target.draw(&fill[0], validsections * 2, sf::TrianglesStrip, states);
		}
	};


	enum class State
	{
		// after the start of the program, or after a reset, or after a timer finished
		NONE,
		// currently in a timer
		COUNTING,
		// a timer is currently paused
		PAUSED,
	};

	enum class TimerType
	{
		POMODORI,
		BREAK,
	};
	duration getTimerDuration(TimerType type)
	{
		switch (type)
		{
		case TimerType::POMODORI: return duration(60 * 25); // 25 min
		case TimerType::BREAK: return duration(60 * 5); // 5 min
		default: throw invalid_argument("invalid timer type");
		}
	}
	ostream &operator<<(ostream& lhs, const State& rhs) {
		switch (rhs)
		{
		case State::NONE: return lhs << "NONE";
		case State::COUNTING: return lhs << "COUNTING";
		case State::PAUSED: return lhs << "PAUSED";
		default: return lhs << "(invalid)";
		}
	}

	// main program
	class Pomodoros {
		sf::RenderWindow window;
		bool needs_redraw;
		duration remainung_until_redraw;

		State state;
		TimerType timertype;

		time_point timer_start, timer_end;
		duration timer_duration; // = timer_end - timer_start
		double progress;

		// alarm things
		sf::SoundBuffer alarm_soundbuffer;
		sf::Sound alarm;

		// drawables
		SquareProgressBar bar;

	public:
		Pomodoros()
			: needs_redraw(true)
			, state(State::NONE)
			, timertype(TimerType::POMODORI)
			, progress(0)
		{
			// create window
			sf::ContextSettings contextsettings;
			contextsettings.antialiasingLevel = 8;
			window.create(sf::VideoMode(200, 200), "woof", sf::Style::Titlebar | sf::Style::Close, contextsettings);

			if (!alarm_soundbuffer.loadFromFile(ALARM_FILE))
			{
				vector<sf::Int16> samples(44100);
				for (auto i = 0; i < 44100; i++) {
					samples[i] = sin(2 * M_PI * i / 30.)
						* (numeric_limits<sf::Int16>::max() - 1);
				}
				alarm_soundbuffer.loadFromSamples(&samples[0], 44100, 1, 44100);
			}
			alarm.setBuffer(alarm_soundbuffer);

			onReset();
		}

		void draw()
		{
			bar.updateVertices();
			window.draw(bar);
		}

		void handleTime(time_point const &now, duration const &delta)
		{
			// maybe redrawing will be necessary
			remainung_until_redraw -= delta;
			if (remainung_until_redraw >= GUI_INTERVAL)
			{
				remainung_until_redraw = duration::zero();
				needs_redraw = true;
			}

			if (state == State::COUNTING)
			{
				progress = 1 - (now - timer_start) / timer_duration;
				bar.progress = static_cast<float>(progress);

				if (now >= timer_end)
				{
					onFinish();
					state = State::NONE;
				}
			}
		}

		void onFinish()
		{
			// todo play sound / notification
			alarm.play();
			onReset();
			switch(timertype)
			{
			case TimerType::POMODORI: timertype = TimerType::BREAK; break;
			case TimerType::BREAK: timertype = TimerType::POMODORI; break;
			default: throw logic_error("no such timer type");
			}
		}
		void onPause()
		{
			bar.updateColor(P_COLOR_PAUSED);
		}
		void onStartOrResume()
		{
			switch (timertype)
			{
			case TimerType::POMODORI: bar.updateColor(P_COLOR_POMODORI); break;
			case TimerType::BREAK: bar.updateColor(P_COLOR_BREAK); break;
			default: break;
			}
		}
		void onReset()
		{
			bar.progress = 1;
			bar.updateColor(P_COLOR_RESET);
		}
		void onSeek(time_point const &now, double amount)
		{
			switch (timertype)
			{
			case TimerType::POMODORI: amount *= SEEKSPEED_POMODORI; break;
			case TimerType::BREAK: amount *= SEEKSPEED_BREAK; break;
			default: throw logic_error("no such timertype");
			}
			updateProgress(now, progress + amount);
		}

		// set the timer_start and timer_end and progress member variables to new values
		void updateProgress(time_point const &now, double progress)
		{
			if (progress < 0) progress = 0;
			if (progress > 1) progress = 1;
			this->progress = progress;
			timer_start = now - timer_duration * (1 - progress);
			timer_end = timer_start + timer_duration;
		}

		void handleSFMLEvent(const sf::Event& ev, time_point const &now, duration const &delta)
		{
			if (ev.type == sf::Event::Closed) window.close();

			if (state == State::NONE) {
				if (ev.type == sf::Event::KeyPressed
					&& (ev.key.code == sf::Keyboard::Space || ev.key.code == sf::Keyboard::Return))
				{
					timer_start = now;
					timer_duration = getTimerDuration(timertype);
					timer_end = timer_start + timer_duration;
					state = State::COUNTING;
					onStartOrResume();
					return;
				}
			}
			if (state == State::COUNTING || state == State::PAUSED) {
				// Either way, BackSpace or Escape revert
				if (ev.type == sf::Event::KeyPressed) {
					if (ev.key.code == sf::Keyboard::BackSpace || ev.key.code == sf::Keyboard::Escape)
					{
						state = State::NONE;
						onReset();
						return;
					}
					if (ev.key.code == sf::Keyboard::Left) onSeek(now, 1);
					if (ev.key.code == sf::Keyboard::Right) onSeek(now, -1);
				}
			}
			if (state == State::COUNTING) {
				if (ev.type == sf::Event::KeyPressed && ev.key.code == sf::Keyboard::Space)
				{
					state = State::PAUSED;
					onPause();
					return;
				}
			}
			if (state == State::PAUSED) {
				if (ev.type == sf::Event::KeyPressed && ev.key.code == sf::Keyboard::Space)
				{
					updateProgress(now, progress);
					state = State::COUNTING;
					onStartOrResume();
					return;
				}
			}
		}

		void eventLoop()
		{
			sf::Event ev;
			time_point framestart, lastframestart;
			lastframestart = std::chrono::steady_clock::now();
			while (window.isOpen()) {
				framestart = std::chrono::steady_clock::now();
				auto delta = framestart - lastframestart;

				while (window.pollEvent(ev))
					handleSFMLEvent(ev, framestart, delta);

				handleTime(framestart, delta);

				// ReSharper disable once CppRedundantBooleanExpressionArgument
				if (needs_redraw || GUI_ALWAYS_REDRAW) {
					window.clear(sf::Color::Black);
					draw();
					window.display();
				}

				// sleep the rest of the interval which we didn't use
				auto now = std::chrono::steady_clock::now();
				auto sleepfor = GUI_INTERVAL - (now - framestart);
				std::this_thread::sleep_for(std::chrono::duration_cast<std::chrono::milliseconds>(sleepfor));
				lastframestart = framestart;
			}
		}
	};
}

int main() {
	pomo::Pomodoros pomodoros;
	pomodoros.eventLoop();
	return 0;
}
