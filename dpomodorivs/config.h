#pragma once

#include <chrono>
#include <SFML/Graphics.hpp>

// the interval (in seconds) to sleep (at most) between pollEvent()s
namespace pomo {
	const auto GUI_INTERVAL = std::chrono::duration<double>(1.0 / 30);
	constexpr auto GUI_ALWAYS_REDRAW = false;

	const auto P_COLOR_POMODORI = sf::Color(255, 0, 0, 255);
	const auto P_COLOR_BREAK = sf::Color(0, 0, 255, 255);
	const auto P_COLOR_PAUSED = sf::Color(127, 127, 127, 255);
	const auto P_COLOR_RESET = sf::Color(31, 31, 31, 255);

	// 0.1 means 10% of 25 minutes
	// >= 1 means just end it
	const auto SEEKSPEED_POMODORI = 0.125;
	const auto SEEKSPEED_BREAK = 0.2;

	const auto ALARM_FILE = "alarm.ogg";
}